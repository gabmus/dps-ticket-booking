#!/bin/bash
make all

SESSION=$USER

tmux -2 new-session -d -s $SESSION

tmux new-window -t $SESSION:1 -n 'Server1'
tmux split-window -h
tmux select-pane -t 0
tmux send-keys "java -cp out org.gabmus.ticketbooking.server.ServerMain" C-m

tmux select-pane -t 1
tmux send-keys "sleep 2 && java -cp out org.gabmus.ticketbooking.client.ClientMain" C-m

tmux split-window -h
tmux select-pane -t 2
tmux send-keys "sleep 2 && java -cp out org.gabmus.ticketbooking.client.ClientMain" C-m

tmux split-window -h
tmux select-pane -t 3
tmux send-keys "sleep 2 && java -cp out org.gabmus.ticketbooking.client.ClientMain" C-m

tmux split-window -h
tmux select-pane -t 4
tmux send-keys "sleep 2 && java -cp out org.gabmus.ticketbooking.client.ClientMain" C-m


tmux -2 attach-session -t $SESSION

