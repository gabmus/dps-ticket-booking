package org.gabmus.ticketbooking.client;

import org.gabmus.ticketbooking.Metadata;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class ClientMain {
    public static void main(String[] argv) {

        int port = Metadata.PORT;
        String address = "127.0.0.1";

        while (true) {
            try {
                Socket clientSocket = new Socket(address, port);

                PrintStream toServer = new PrintStream(clientSocket.getOutputStream());
                Scanner fromServerScanner = new Scanner(clientSocket.getInputStream());

                toServer.println(Metadata.TICKET_REQUEST_MSG);
                String result = fromServerScanner.nextLine();
                if (result.toLowerCase().contains("no more")) {
                    System.err.println(result);
                    fromServerScanner.close();
                    clientSocket.close();
                } else {
                    System.out.println(result);
                    fromServerScanner.close();
                    clientSocket.close();
                }
                break;
            }
            catch (IOException e) {
                System.err.println("Connection error, retrying");
            }
        }
    }
}
