package org.gabmus.ticketbooking.server;

public class TicketSystem {

    private int numTickets;

    public TicketSystem(int numTickets) {
        this.numTickets = numTickets;
    }

    /*
    public synchronized boolean isAvailable() {
        // simulating database access
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return numTickets > 0;
    }*/

    public synchronized int getTicket() {
        if (numTickets > 0) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            numTickets--;
        }
        return numTickets;
    }
}
