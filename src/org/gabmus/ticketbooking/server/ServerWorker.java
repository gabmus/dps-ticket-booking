package org.gabmus.ticketbooking.server;

import org.gabmus.ticketbooking.Metadata;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class ServerWorker implements Runnable {

    private Socket socket;
    private TicketSystem ticketSystem;

    public ServerWorker(Socket socket, TicketSystem ticketSystem) {
        this.socket = socket;
        this.ticketSystem = ticketSystem;
    }

    @Override
    public void run() {
        try {
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintStream outToClient = new PrintStream(socket.getOutputStream());
            String data = inFromClient.readLine();
            if (data.contains(Metadata.TICKET_REQUEST_MSG)) {
                int ticket = ticketSystem.getTicket();
                outToClient.println(
                        ticket == 0 ?
                                "No more tickets available" :
                                "You got ticket number " + Integer.toString(ticket)
                );
            }
            else {
                outToClient.println("Wrong message");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            socket.close();

        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
