out/%.class: src/%.java
	javac -d out -cp out src/$*.java

SOURCES = src/org/gabmus/ticketbooking/Metadata.java \
		src/org/gabmus/ticketbooking/ErrorHandler.java \
		src/org/gabmus/ticketbooking/server/TicketSystem.java \
		src/org/gabmus/ticketbooking/server/ServerWorker.java \
		src/org/gabmus/ticketbooking/client/ClientMain.java \
		src/org/gabmus/ticketbooking/server/ServerMain.java \

all: $(patsubst src/%,out/%,$(patsubst %.java,%.class,$(SOURCES)))

clean:
	rm -rf out/*
